//SUMA

//DECLARACIÓN DE FUNCIÓN
function suma (n1, n2) {
    console.log(n1 + n2);
}
suma(30, 20);

//EXPRESIÓN DE FUNCIÓN
let sumaResultado = function suma(n1, n2) {
    console.log(n1 + n2);
}
sumaResultado(40, 60);

//FUNCIÓN FLECHA
let sumaResultado2 = (n1, n2) => {
    console.log(n1 + n2);
}
sumaResultado2(891, 88);

//____________________________________________________________________________________________
//RESTA

//DECLARACIÓN DE FUNCIÓN
function sustraccion (n1, n2) {
    console.log(n1 - n2);
}
sustraccion(200, 40);

//EXPRESIÓN DE FUNCIÓN
let sustraccion1 = function sustraccion(n1, n2) {
    console.log(n1 - n2);
}
sustraccion1(50, 25);

//FUNCIÓN FLECHA
let sustraccion2 = (n1, n2) => {
    console.log(n1 - n2);
}
sustraccion2(300, 150);

//___________________________________________________________________________________________
//MULTIPLICACIÓN

//DECLARACIÓN DE FUNCIÓN
function multiplicacion (n1, n2) {
    console.log(n1 * n2);
}
multiplicacion(3, 30);

//EXPRESIÓN DE FUNCIÓN
let multResultado = function multiplicacion(n1, n2) {
    console.log(n1 * n2);
}
multResultado(4,15);

//FUNCIÓN FLECHA
let multResultado2 = (n1, n2) => {
    console.log(n1 * n2);
}
multResultado2(5, 20);

//___________________________________________________________________________________________
//DIVISIÓN

//DECLARACIÓN DE FUNCIÓN
function division (n1, n2) {
    console.log(n1 / n2);
}
division(4,16);

//EXPRESIÓN DE FUNCIÓN
let divResultado = function division(n1, n2) {
    console.log(n1 / n2);
}
divResultado(90,3);

//FUNCIÓN FLECHA
let divResultado2 = (n1, n2) => {
    console.log(n1 / n2);
}
divResultado2(75,5);